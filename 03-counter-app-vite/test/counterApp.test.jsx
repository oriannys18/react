import {  fireEvent, render, screen} from "@testing-library/react";
import { CounterApp } from "../src/CounterApp";

describe('pruebas en <CounterApp>', () => { 
  const initialvalue = 10;


    test('debe hacer math en el snapsthot', () => { 

      const title = 'Hola. soy oriana';

      const {container} = render( <CounterApp value={ initialvalue} /> )
      expect(container).toMatchSnapshot();

    });

    test('debe de mostrar el valor inicial de 100 <CounterApp value{100}/>', () => { 

      const title = 'Hola. soy oriana';
  
      render( <CounterApp value={ 100 } /> );
      expect(screen.getByText(100)).toBeTruthy();

      expect(screen.getByRole('heading',{ level: 2 } ).innerHTML ).toContain('100');
        
   
          
    });

       
    test('debe de incrementar con el boton +1', () => { 

                          
      render( <CounterApp value={ initialvalue } /> );
      fireEvent.click( screen.getByText('+1') );
      expect(screen.getByText(11)).toBeTruthy();

    });

    test('debe de decrementar  el boton -1', () => { 

                          
      render( <CounterApp value={initialvalue} /> );
      fireEvent.click( screen.getByText('-1') );
      expect(screen.getByText(9)).toBeTruthy();
  
      
    });

    test('debe de funcionar el boton de reset', () => { 

                          
      render( <CounterApp value={ 355 } /> );
      fireEvent.click( screen.getByText('+1') );
      fireEvent.click( screen.getByText('+1') );
      fireEvent.click( screen.getByText('-1') );

      //fireEvent.click( screen.getByText('reset') );
       
      // expect( screen.getByText(355)).toBeTruthy();
      fireEvent.click( screen.getByRole('button', { name: 'btn-reset' }));

    
    });

});