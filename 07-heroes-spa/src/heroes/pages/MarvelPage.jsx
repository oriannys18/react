import { HeroList } from "../components"


export const MarvelPage = () => {
  return (

    <>
    
      <h1 className="animate__animated animate__zoomIn">MarvelPage</h1>
      <hr />

      <HeroList publisher='Marvel Comics' />

    </>
  )
}
