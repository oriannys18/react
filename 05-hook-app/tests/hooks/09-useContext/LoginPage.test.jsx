import { screen, render, fireEvent } from "@testing-library/react";
import { UserContext } from "../../../src/09-useContex/Context/UserContext";
import { LoginPage } from "../../../src/09-useContex/LoginPage";

describe('pruebas en el componente LoginPage', () => { 


    test('debe de mostrar el componente sin el usuario', () => { 

        render(
            <UserContext.Provider value={{ user: null }}>
                <LoginPage />
            </UserContext.Provider>
        );

        const pre = screen.getByLabelText('pre');
        expect(pre.innerHTML).toBe('null');

    });

    test('debe de llamar el setUser cuando se hace click en el boton', () => { 

        const setUserMock = jest.fn();

        render(
            <UserContext.Provider value={{ user: null, setUser: setUserMock }}>
                <LoginPage />
            </UserContext.Provider>
        );

        const button = screen.getByRole('button');
        fireEvent.click( button );
        expect( setUserMock ).toHaveBeenCalled();
        expect( setUserMock ).toHaveBeenCalledWith({ "email": "orianaismar@gmail.com", "id": 123, "name": "juan" });
        
    });

});