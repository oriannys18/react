import { render } from "@testing-library/react"
import { FirstApp } from "../src/FirstApp"



describe('pruebas en </FirstApp>', () => { 

    test('debe hacer math en el snapsthot', () => { 

      //  const title = 'Hola. soy oriana';

      // const {container} = render( <FirstApp title={ title } /> )
      // expect(container).toMatchSnapshot();

        
      });

     test('debe de mostar el titulo de h1', () => { 

        const title = 'Hola, soy oriana';

       const {container, getByText, getByTestId} = render( <FirstApp title={ title } /> )
       expect(getByText(title)).toBeTruthy();

      // const h1 = container.querySelector('h1');
      // expect(h1.innerHTML).toContain( title)
        expect(getByTestId('maria')).toBeTruthy();
        expect(getByTestId('maria').innerHTML).toContain(title);
       

        
     });

     test('debe de mostrar el subtitulo enviado por props', () => { 

        const title = 'Hola, soy oriana';
        const subTitle = 'soy un subtitulo';
       const { getAllByText} = render( 
       
         <FirstApp 
            title={ title }
            subTitle={subTitle}

         /> 
         );

       expect( getAllByText(subTitle).length).toBe(2);

 });


  });