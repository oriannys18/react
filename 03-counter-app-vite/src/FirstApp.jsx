/* const getSaludo = (n1, n2) => {

    
    return  n1 + n2;
    
} */

import PropTypes from 'Prop-Types';


export const FirstApp= ({title, subTitle, name}) => {
    //console.log(title);

    return (

        <>

         <h1 data-testid="maria"> { title } </h1>

         <p>{ subTitle }</p>
         <p>{ subTitle }</p>

         <h2>{ name }</h2>
        
        </>
     
    )
  }

  FirstApp.propTypes = {
      title: PropTypes.string.isRequired,
      subTitle:PropTypes.string.isRequired,
      name: PropTypes.string.isRequired,
      
  }
   
  FirstApp.defaultProps = {

    name: 'no hay nombre',
    subTitle: 'no hay subtitulo',
    title: 'no hay titulo',
  }