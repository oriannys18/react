import { render,screen } from "@testing-library/react"
import { GifGrid } from "../../src/components/GifGrid"
import { useFetchGifs } from "../../src/hooks/useFetchGifs"

jest.mock( '../../src/hooks/useFetchGifs' )

describe('Pruebas en GifGrid', () => { 

    const category = 'One Puch'

    test('Debe de mostrar el loading inicialmente', () => { 

        const gifs = [
            {
                id: 'ABC',
                title: 'Saitama',
                url:'https://localhost/saitama.jpg'
            },
            {
                id: '123',
                title: 'Goku',
                url:'https://localhost/goku.jpg'
            },
        ]

        useFetchGifs.mockReturnValue({
            images: gifs,
            isLoading: true
        });

        render( < GifGrid category={ category } /> );
        
        expect(screen.getByText( 'Cargando...' ));
        expect(screen.getAllByRole( 'img' ).length).toBe(2);
    })

    test('debe de mostrar items cuendo se cargan las imagenes UseFetchGifs', () => { 



     })
 })