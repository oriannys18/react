import { useState } from 'react';
import PropTypes from 'Prop-types';

export const CounterApp = ({ value} ) => {

    const [ counter, setCounter ]  = useState( value );

    const hadleAdd = () => {
        
        setCounter( counter + 1 );
        //* setCounter( (c) => c + 1) esta es otro forma de hacerlo, o un callback*//
        
    }

    const hadleRest = () => setCounter( counter - 1 );
        
     const hadleReset = () =>  setCounter( value );
        
    return (
        <>
    
            <h1> CounterApp </h1>
            <h2> { counter } </h2>
            
            <button onClick={ hadleAdd }> +1 </button>

            <button onClick={ hadleRest }> -1 </button>

            <button aria-label='btn-reset' onClick={ hadleReset }> reset </button>
    
        </> 

    );
    

}

CounterApp.propTypes = {
  value: PropTypes.number
}