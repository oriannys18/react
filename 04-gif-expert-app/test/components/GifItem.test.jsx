import { GifItem } from "../../src/components/GifItem";
import { render, screen } from "@testing-library/react"

describe('pruebas a GifItem', () => {

    const title ='good morning'
    const url = 'https://good-morning.com/good%20morning.jpg';

    test(' Hace el Snapshot ', () => { 
        
        const { container } = render( <GifItem title={ title } url={ url }  /> )
        expect(container).toMatchSnapshot();
        
    })  

    test(' Ddebe de mostrar la imagen con el URL y el ALT indicado', () => { 
        
         render( <GifItem title={ title } url={ url }  /> )
        
        //expect(screen.getByRole('img').src).toBe( url );
        const {src, alt } = screen.getByRole('img');
        expect( src ).toBe( url );
        expect( alt ).toBe( alt );
    });  

    test(' Debe de mostrar el titulo en el componente', () => { 
        
         render( <GifItem title={ title } url={ url }  /> )
        
        expect( screen.getByText( title ) ).toBeTruthy();
    });  


})