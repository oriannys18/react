import { getImagen } from "../src/base-pruebas/11-async-await";

describe('prueba en 11-async-await.js', () => { 

    test('getImagen debe de retornar un URL de la imagen', async () => { 

        const url = await getImagen();
        //console.log(url);
        expect(url).toMatch(/https/);
    })

})