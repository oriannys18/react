import { UserProvider } from "./Context/UserProvider"
import { Navigate, Route, Routes } from "react-router-dom"
import { AboutPage } from "./AboutPage"
import { HomePage } from "./HomePage"
import { LoginPage } from "./LoginPage"
import { Navbar } from "./Navbar"


export const MainApp = () => {
    return (
        <UserProvider>
            {/* <h1>MainApp</h1> */}
            <Navbar />
            
            <hr />

            <Routes>
                <Route path="/" element={<HomePage />} />
                <Route path="about" element={<AboutPage />} />
                <Route path="login" element={<LoginPage />} />


                { /*<Route path="/*" element={ <LoginPage /> } />*/}
                <Route path="/*" element={<Navigate to="/about" />} />
                
            </Routes>

        </UserProvider>
        //como enviar al usuario si pone una direccion que no existe. es decir, con Navigate se pone la 
        //pagina que uno quier, si el usuario pone una dirrecion qie no existe uno lo direcciona a una que si existe.
    )
}
