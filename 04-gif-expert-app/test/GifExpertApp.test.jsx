import { render, screen, fireEvent } from "@testing-library/react"
import { GifExpertApp } from "../src/GifExpertApp"

describe('pruebas en GifExpertApp', () => { 

    test('debe cambiar el value en la caja de texto', () => { 

        const onAddCategory = jest.fn();

        render( <GifExpertApp onAddCategory ={ onAddCategory } /> )
        const input = screen.getByRole('textbox');
       const form = screen.getByRole('form');
    
        fireEvent.input( input, { target: { value: 'Good morning' } } );
        fireEvent.submit( form );

        expect( input.value ).toBe( '');

        screen.debug();
     });

     



});