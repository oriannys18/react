import { render, screen } from "@testing-library/react";
import { UserContext } from "../../../src/09-useContex/Context/UserContext";
import { HomePage } from "../../../src/09-useContex/HomePage";

describe('pruebas en el componente HomePage', () => { 

    const user = {
        id: 1,
        name: 'oriana'
    }
    test('debe de mostrar el componenete sin el usuario', () => { 

        render(
            <UserContext.Provider value={{ user: null }}>
                <HomePage />
            </UserContext.Provider>
        );

        const preTag = screen.getByLabelText('pre');
        expect( preTag.innerHTML ).toBe( 'null' );

    });

    test('debe de mostrar el componenete con el usuario', () => { 

        render(
            <UserContext.Provider value={{ user: user }}>
                <HomePage />
            </UserContext.Provider>
        );

        const preTag = screen.getByLabelText('pre');
        expect( preTag.innerHTML ).toContain( user.name );
        expect( preTag.innerHTML ).toContain( user.name.toString() );
    });
});