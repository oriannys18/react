
import { getHeroeById, getHeroesByOwner } from '../src/base-pruebas/08-imp-exp';
import heroes from '../src/data/heroes';


describe('pruebas en 08-imp-exp', () => { 
    test('getHeroeById debe retornar un heroe por ID', () => { 

        const id = 1;
        const hero = getHeroeById( id );

        expect( hero ).toEqual({id: 1,
            name: 'Batman',
            owner: 'DC'

        })

     })

     test('si hay un heroe qu eno exite debe retornar undefined', () => { 

        const id = 100;
        
        const hero = getHeroeById( id );

        expect( hero ).toBeFalsy();
      });

      test('debe de retornar los heroes de DC', () => { 

        const owner = 'DC';
        const heroe = getHeroesByOwner(owner)

        expect( heroe.length).toEqual( 3 )

        expect( heroe ).toEqual( [ 
           { id: 1, name: 'Batman', owner: 'DC' },
           { id: 3, name: 'Superman', owner: 'DC' },
            { id: 4, name: 'Flash',owner: 'DC' }
        ]);

        expect( heroe ).toEqual(heroes.filter( (heroe) => heroe.owner === owner ));

       });

       test('debe de retornar los heroes de Marvel', () => { 

        const owner = 'Marvel';
        const heroe = getHeroesByOwner(owner);

        expect( heroe.length ).toBe( 2 );

        expect( heroe ).toEqual(heroes.filter( (heroe) => heroe.owner === owner ));

       });
       



 });