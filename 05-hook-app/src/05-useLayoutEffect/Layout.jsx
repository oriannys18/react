import { useCounter, useFetch } from "../hooks";
import { LoadingQuate, Quote } from "../03-examples";


export const Layout = () => {

    const {counter, increment} = useCounter(1);

    const { data, isLoading, hasError } = useFetch(`https://www.breakingbadapi.com/api/quotes/${ counter }`);
    const { author, quote } = !!data && data[0];

  return (
    <>
      <h1>BrekingBad Quotes</h1>
      <hr />

      {
        isLoading
          ? <LoadingQuate />
          : <Quote author={ author } quote={ quote }/>
      }

      <button
        className="btn btn-primary"
        disabled={isLoading}
        onClick={() => increment()}>
        Next quote
      </button>

    </>
  )
}
