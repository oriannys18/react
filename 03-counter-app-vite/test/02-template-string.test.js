import { getSaludo } from "../src/base-pruebas/02-template-string";







describe('prueba  template-string', () => { 

    test('getSaludo debe retornar "Hola oriana"', () => { 

        const name = 'oriana';

        const message = getSaludo( name );

        expect( message ).toBe(`Hola ${ name }`)


     });



 });
